from .common import *


DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": ":memory:",
    }
}
SECRET_KEY = env('DJANGO_SECRET_KEY', default='asdfasdfasdfasdfasdfasdf234234af')
# https://stackoverflow.com/questions/13705328/how-to-run-django-tests-on-heroku


