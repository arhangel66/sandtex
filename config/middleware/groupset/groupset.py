# -*- coding: utf8 -*-
from sandtex.users.models import Group

__author__ = 'Derbichev Mikhail, arhangel662@gmail.com'

import pytz

from django.utils import timezone


class GroupSetMiddleware(object):
    def process_request(self, request):
        if request.user.id:  # and request.session.get('tz') != request.user.account.timezone:
            if not 'group' in request.session:
                group = request.user.group
                if not group:
                    groups = Group.objects.filter().order_by('id')
                    if groups.exists():
                        group = groups[0]
                    else:
                        group = Group.objects.create(name='Gold')
                    request.user.group = group
                    request.user.save()
                request.session['group'] = {'id': group.id, 'name': group.name}
        else:
            pass
            #timezone.activate(pytz.timezone('UTC'))
