# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.views.generic import TemplateView
from django.views import defaults as default_views

from sandtex.main import views
from sandtex.history import views as hist_views
from sandtex.main.files.views import DeleteFile, GetFiles, ViewFile, EditFile, PreViewFile

urlpatterns = [
                  # url(r'^$', TemplateView.as_view(template_name='pages/home.html'), name='home'),
                  url(r'^$', views.DashboardView.as_view(), name='dashboard'),
                  url(r'^customer/create/$', views.CustomerCreateView.as_view(), name='customer-create'),
                  url(r'^vendor/create/$', views.VendorCreateView.as_view(), name='vendor-create'),
                  url(r'^vendor/(?P<pk>\d+)/update/$', views.VendorUpdateView.as_view(), name='vendor-update'),
                  url(r'^setgroup/$', views.SetGroup.as_view(), name='group-set'),

                  url(r'^bids/$', views.BidsView.as_view(), name='bids'),
                  url(r'^bids/create/$', views.BidCreateView.as_view(), name='bid-create'),
                  url(r'^bids/(?P<pk>\d+)/update/$', views.BidUpdateView.as_view(), name='bid-update'),
                  url(r'^bids/(?P<pk>\d+)/history/$', hist_views.BidHistoryView.as_view(), name='bid-history'),
                  url(r'^bids/(?P<pk>\d+)/close/$', views.BidCloseView.as_view(), name='bid-close'),

                  url(r'^bids/(?P<pk>\d+)/items/add/$', views.ItemCreateView.as_view(), name='item-create'),
                  url(r'^bids/(?P<bid_pk>\d+)/items/(?P<pk>\d+)/edit/$', views.ItemUpdateView.as_view(),
                      name='item-update'),
                  url(r'^bids/(?P<bid_pk>\d+)/items/(?P<pk>\d+)/delete/$', views.ItemDeleteView.as_view(),
                      name='item-delete'),

                  url(r'^bids/(?P<bid_pk>\d+)/rfq/manage/$', views.RfqCreateView.as_view(), name='rfq-create'),
                  url(r'^bids/(?P<bid_pk>\d+)/rfq/(?P<pk>\d+)/delete/$', views.RfqDeleteView.as_view(),
                      name='rfq-delete'),
                  # url(r'^about/$', TemplateView.as_view(template_name='pages/about.html'), name='about'),
                  url(r'^bids/(?P<bid_pk>\d+)/rfq/(?P<rfq_pk>\d+)/pdf/$', views.MyPdfView.as_view(), name='rfq-pdf'),
                  url(r'^bids/(?P<bid_pk>\d+)/rfq/(?P<rfq_pk>\d+)/pdf/get/$', views.GetPdfView.as_view(), name='rfq-pdf-download'),
                  url(r'^bids/(?P<bid_pk>\d+)/rfq/(?P<rfq_pk>\d+)/send/$', views.SendRfq.as_view(), name='rfq-send'),


                  url(r'^file/(?P<pk>\d+)/del/$', DeleteFile.as_view(), name='del_file'),

                  url(r'^file/(?P<bid_pk>\d+)/edit/$', EditFile.as_view(), name='edit_file'),
                  url(r'^file/(?P<bid_pk>\d+)/view/$', ViewFile.as_view(), name='view_file'),
                  url(r'^file/(?P<bid_pk>\d+)/preview/$', PreViewFile.as_view(), name='preview_file'),
                  url(r'^files/(?P<bid_pk>\d+)/$', GetFiles.as_view(), name='get_files'),
                  # url(r'^file/preview/$', PreViewFile.as_view(), name='preview_file'),

                  # Django Admin, use {% url 'admin:index' %}
                  url(settings.ADMIN_URL, include(admin.site.urls)),

                  # User management
                  url(r'^users/', include('sandtex.users.urls', namespace='users')),
                  url(r'^accounts/', include('allauth.urls')),

                  # Your stuff: custom urls includes go here

              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    urlpatterns += [
        url(r'^400/$', default_views.bad_request, kwargs={'exception': Exception('Bad Request!')}),
        url(r'^403/$', default_views.permission_denied, kwargs={'exception': Exception('Permission Denied')}),
        url(r'^404/$', default_views.page_not_found, kwargs={'exception': Exception('Page not Found')}),
        url(r'^500/$', default_views.server_error),
    ]
