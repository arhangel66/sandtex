# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2017-08-23 08:09
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('main', '0012_auto_20170823_0515'),
    ]

    operations = [
        migrations.CreateModel(
            name='HistoricalBidItem',
            fields=[
                ('id', models.IntegerField(auto_created=True, blank=True, db_index=True, verbose_name='ID')),
                ('created', django_extensions.db.fields.CreationDateTimeField(auto_now_add=True, verbose_name='created')),
                ('qty', models.IntegerField(blank=True, null=True, verbose_name='quantity')),
                ('descr_span', models.CharField(blank=True, max_length=200, null=True, verbose_name='description in spanish')),
                ('brand', models.CharField(blank=True, max_length=100, null=True, verbose_name='brand')),
                ('descr_eng', models.CharField(blank=True, max_length=200, null=True, verbose_name='description in english')),
                ('is_open', models.BooleanField(default=True)),
                ('weight', models.CharField(blank=True, max_length=50, null=True, verbose_name='Weight')),
                ('lead_time', models.CharField(blank=True, max_length=50, null=True, verbose_name='Lead Time')),
                ('notes', models.TextField(blank=True, null=True, verbose_name='Notes')),
                ('best_quote', models.CharField(blank=True, max_length=200, null=True, verbose_name='Best Quote')),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_change_reason', models.CharField(max_length=100, null=True)),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
                ('author', models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to=settings.AUTH_USER_MODEL)),
                ('bid', models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='main.Bid')),
                ('history_user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'historical bid item',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
        ),
        migrations.CreateModel(
            name='HistoricalRfq',
            fields=[
                ('id', models.IntegerField(auto_created=True, blank=True, db_index=True, verbose_name='ID')),
                ('created', django_extensions.db.fields.CreationDateTimeField(auto_now_add=True, verbose_name='created')),
                ('gen_file', models.TextField(max_length=100, verbose_name='/')),
                ('email', models.CharField(blank=True, max_length=150, null=True, verbose_name='email')),
                ('is_send', models.BooleanField(default=False, verbose_name='is_send')),
                ('send_dt', models.DateTimeField(blank=True, null=True, verbose_name='send datetime')),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_change_reason', models.CharField(max_length=100, null=True)),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
                ('author', models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to=settings.AUTH_USER_MODEL)),
                ('bid', models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='main.Bid')),
                ('history_user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL)),
                ('vendor', models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='main.Vendor')),
            ],
            options={
                'verbose_name': 'historical rfq',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
        ),
        migrations.RemoveField(
            model_name='historicalbid',
            name='modified',
        ),
    ]
