# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2017-07-30 05:00
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0010_bid_close_dt'),
    ]

    operations = [
        migrations.AddField(
            model_name='biditem',
            name='best_quote',
            field=models.CharField(blank=True, max_length=200, null=True, verbose_name='Best Quote'),
        ),
        migrations.AddField(
            model_name='biditem',
            name='lead_time',
            field=models.CharField(blank=True, max_length=50, null=True, verbose_name='lead_time'),
        ),
        migrations.AddField(
            model_name='biditem',
            name='notes',
            field=models.TextField(blank=True, null=True, verbose_name='Notes'),
        ),
        migrations.AddField(
            model_name='biditem',
            name='weight',
            field=models.CharField(blank=True, max_length=50, null=True, verbose_name='Weight'),
        ),
    ]
