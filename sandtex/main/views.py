import calendar
import json
from datetime import date, timedelta, datetime

from braces.views import CsrfExemptMixin
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.urlresolvers import reverse, reverse_lazy
from django.db.models import Q
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import redirect
from django.views.generic import TemplateView, ListView, DetailView, UpdateView, DeleteView, FormView, View
from django.views.generic.edit import CreateView

from sandtex.main import forms
from sandtex.main.models import Bid, BidItem, Rfq, Vendor
from sandtex.olddb.models import Tblsalesummaries
from sandtex.users.models import Group


def linreg(X, Y):
    """
    return a,b in solution to y = ax + b such that root mean square distance between trend line and original points is minimized
    """
    N = len(X)
    Sx = Sy = Sxx = Syy = Sxy = 0.0
    for x, y in zip(X, Y):
        Sx = Sx + x
        Sy = Sy + y
        Sxx = Sxx + x * x
        Syy = Syy + y * y
        Sxy = Sxy + x * y
    det = Sxx * N - Sx * Sx
    if det:
        return (Sxy * N - Sy * Sx) / det, (Sxx * Sy - Sx * Sxy) / det
    else:
        return 0, 0


def get_first_day(dt, d_years=0, d_months=0):
    # d_years, d_months are "deltas" to apply to dt
    y, m = dt.year + d_years, dt.month + d_months
    a, m = divmod(m - 1, 12)
    return date(y + a, m + 1, 1)


def get_last_day(dt):
    return get_first_day(dt, 0, 1) + timedelta(-1)


class DashboardView(LoginRequiredMixin, TemplateView):
    template_name = 'pages/dashboard.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        # filename = "/Users/mikhail/w/Churn analysis.xlsx"
        # open_file(filename)
        context['month_sales'] = json.dumps(self.make_month_list())
        context['year_sales'] = json.dumps(self.make_year_list())
        context['open_bids'] = json.dumps(self.open_bids())
        context['month'] = calendar.month_name[datetime.now().month]
        context['year'] = datetime.now().year
        context['last_year'] = datetime.now().year - 1
        # print(context['year_sales'])
        return context

    def make_month_list(self):
        now = datetime.now()  # + timedelta(days=-5)
        first = get_first_day(now)
        curday = first
        last = get_last_day(now)
        last_year = 0
        val_dict = {}
        date_list = []
        prev = 0
        goal = 0

        all_values = Tblsalesummaries.objects.filter(generateddate__range=(first, last)) \
            .values('generateddate',
                    'currentmonth',
                    'currentmonthlastyear').order_by('currentmonth')
        for value in all_values:
            val_dict[str(value['generateddate'].date())] = {'current_month': value['currentmonth']}
            last_year = value['currentmonthlastyear']
            goal = last_year * 1.15

        cur_month_list = []
        while True:
            cur_month = val_dict.get(str(curday), {}).get('current_month')
            if not cur_month and curday < now.date():
                cur_month = prev

            if cur_month:
                cur_month_list.append(cur_month)
                cur_month = round(cur_month)

            prev = cur_month
            date_list.append({'current_month': cur_month,
                              'last_year': round(last_year),
                              'goal': round(goal),
                              'period': str(curday)})
            curday += timedelta(days=1)
            if curday > last:
                break

        # calc trend
        a, b = linreg(range(len(cur_month_list)), cur_month_list)
        for i, dat in enumerate(date_list):
            dat['trend'] = round(a * i + b)

        return date_list

    def make_year_list(self):
        from sandtex.olddb.models import Tblsalesummaries
        date_list = []
        prev_year = 0
        data = Tblsalesummaries.objects.filter(generateddate__year='2016').values('generateddate', 'currentyear',
                                                                                  'previousyear').order_by('id')
        months = range(1, 13)
        cyear_dict = {}
        cyear_list = []
        for line in data:
            cyear_dict[line['generateddate'].month] = line['currentyear']
            prev_year = line['previousyear']

        for month in months:
            cyear = None
            if cyear_dict.get(month):
                cyear = round(cyear_dict.get(month))
                cyear_list.append(cyear)
            date_list.append({'period': "%s-%s" % (datetime.now().year, month),
                              'current': cyear,
                              'prev_year': round(prev_year),
                              'goal': round(prev_year * 1.2)})

        # calc trend
        a, b = linreg(range(len(cyear_list)), cyear_list)
        for i, dat in enumerate(date_list):
            dat['trend'] = round(a * i + b)

        return date_list

    def open_bids(self):
        from datetime import datetime, timedelta
        days_ago = datetime.now() + timedelta(days=-30)

        # TODO add group filter
        all_bids = Bid.objects.filter(Q(group=self.request.session['group']['id']),
                                      Q(created__lte=days_ago, close_dt__gte=days_ago) | Q(
                                          created__gte=days_ago)).values('created', 'close_dt')
        date_list = []
        for i in range(31):
            dat = days_ago + timedelta(days=i)
            open_bids = 0
            for bid in all_bids:
                if bid['created'] < dat and (bid['close_dt'] is None or bid['close_dt'] >= dat):
                    open_bids += 1
            if open_bids == 0 and len(date_list) == 0:
                continue
            date_list.append({'period': str(dat.date()), 'bids': open_bids})
        return date_list


class BidsView(LoginRequiredMixin, ListView):
    template_name = 'pages/bids.html'
    model = Bid
    context_object_name = 'bids'

    def get_queryset(self):

        return Bid.objects.filter(is_open=True, group=self.request.session['group']['id'])


class BidCreateView(LoginRequiredMixin, CreateView):
    template_name = 'pages/bid-create.html'
    form_class = forms.BidCreateForm

    def form_valid(self, form):
        bid = form.save(commit=False)
        bid.author = self.request.user
        bid.group = self.request.user.group
        bid.save()
        return redirect(bid.get_absolute_url())


class UpdateFilesMixin(object):
    def get_context_data(self, **kwargs):
        self.object.compare_files()
        return super(UpdateFilesMixin, self).get_context_data(**kwargs)

class BidUpdateView(LoginRequiredMixin, CsrfExemptMixin, UpdateFilesMixin, DetailView):
    template_name = 'pages/bid-update.html'
    model = Bid
    context_object_name = 'bid'

    def post(self, request, *args, **kwargs):
        if self.request.FILES and 'file' in self.request.FILES:
            file_field = request.FILES['file']
            from sandtex.main.files.views import handle_uploaded_file
            file_obj = handle_uploaded_file(
                file_field,
                request.user.username,
                self.kwargs.get('pk')
            )
            return HttpResponse('Ok')


            # def get_context_data(self, **kwargs):
            #     context = super().get_context_data()
            #     context['bid_pk'] = self.kwargs.get('pk')
            #     return context


class RfqCreateView(LoginRequiredMixin, FormView):
    template_name = 'pages/rfq-creation.html'
    form_class = forms.RfqCreateForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['bid_pk'] = self.kwargs.get('bid_pk')
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['bid_pk'] = self.kwargs.get('bid_pk')
        context['rfq'] = Rfq.objects.filter(bid=self.kwargs.get('bid_pk'))
        return context

    def form_valid(self, form):
        form.save(self.kwargs.get('bid_pk'), author=self.request.user)
        return redirect(reverse('rfq-create', kwargs={'bid_pk': self.kwargs.get('bid_pk')}))


class ItemCreateView(LoginRequiredMixin, CreateView):
    template_name = 'blocks/item-create.html'
    form_class = forms.ItemCreateForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['bid_pk'] = self.kwargs.get('pk')
        return context

    def form_valid(self, form):
        item = form.save(commit=False)
        item.author = self.request.user
        item.bid_id = self.kwargs.get('pk')
        item.save()
        return redirect(item.get_bid_url())


class ItemUpdateView(LoginRequiredMixin, UpdateView):
    template_name = 'blocks/blank-form.html'
    form_class = forms.ItemUpdateForm
    model = BidItem

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['bid_pk'] = self.kwargs.get('bid_pk')
        return context

    def form_valid(self, form):
        item = form.save(commit=False)
        item.best_quote = form.cleaned_data.get('best_quote')
        item.save()
        return redirect(item.get_bid_url())


class ItemDeleteView(LoginRequiredMixin, DeleteView):
    model = BidItem
    template_name = 'pages/item-delete.html'

    def get_success_url(self):
        return self.object.get_bid_url()


class RfqDeleteView(LoginRequiredMixin, DeleteView):
    model = Rfq
    template_name = 'pages/rfq-delete.html'

    def get_success_url(self):
        return self.object.get_absolute_url()


from easy_pdf.views import PDFTemplateView


class MyPdfView(PDFTemplateView):
    template_name = "pages/pdf.html"
    # template_name = 'email/rfq.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        bid = Bid.objects.get(id=kwargs.get('bid_pk'))
        rfq = Rfq.objects.get(id=kwargs.get('rfq_pk'))
        context['bid'] = bid
        context['rfq'] = rfq
        return context


class GetPdfView(MyPdfView):
    def get_pdf_filename(self):
        rfq = Rfq.objects.get(id=self.kwargs.get('rfq_pk'))
        return rfq.get_filename()


class CustomerCreateView(LoginRequiredMixin, CreateView):
    template_name = 'blocks/blank-form.html'
    form_class = forms.CustomerCreateForm
    success_url = reverse_lazy('bid-create')

    def form_valid(self, form):
        item = form.save()
        return redirect(self.success_url)


class BidCloseView(LoginRequiredMixin, DeleteView):
    model = Bid
    template_name = 'pages/bid-close.html'
    success_url = reverse_lazy('bids')

    def delete(self, request, *args, **kwargs):
        """
        Calls the delete() method on the fetched object and then
        redirects to the success URL.
        """

        self.object = self.get_object()
        self.object.close()
        return redirect(self.get_success_url())


class VendorCreateView(LoginRequiredMixin, CreateView):
    template_name = 'blocks/blank-form.html'
    form_class = forms.VendorCreateForm

    def get_success_url(self):
        return self.request.META.get('HTTP_REFERER')

    def from_valid(self, form):
        item = form.save()
        return redirect(self.request.META.get('HTTP_REFERER'))


class VendorUpdateView(LoginRequiredMixin, UpdateView):
    template_name = 'blocks/blank-form.html'
    form_class = forms.VendorUpdateForm
    model = Vendor

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        return context

    def form_valid(self, form):
        vendor = form.save()
        print(vendor.email)
        return HttpResponseRedirect(self.request.META.get('HTTP_REFERER'))


class SetGroup(LoginRequiredMixin, View):
    def get(self, request):
        group_id = self.request.GET.get('group')
        if group_id:
            group = Group.objects.filter(id=group_id)
            if group.exists():
                self.request.session['group'] = {'id': group[0].id, 'name': group[0].name}
        return redirect(self.request.META.get('HTTP_REFERER'))


class SendRfq(LoginRequiredMixin, View):
    def get(self, request, bid_pk, rfq_pk, **kwargs):

        rfq = Rfq.objects.get(id=rfq_pk)
        if not rfq.is_vendor_email_good():
            messages.add_message(request, messages.ERROR, 'Cant send RFQ. Set email address for this vendor.')
        elif rfq.is_send:
            messages.add_message(request, messages.INFO, 'Email not sent because it was sent before')
        else:
            rfq.send_html_email()
            messages.add_message(request, messages.SUCCESS, 'RFQ Sended')
        return HttpResponseRedirect(reverse('rfq-create', kwargs={'bid_pk': rfq.bid_id}))
