import pytest
from django.core.urlresolvers import reverse
from mixer.backend.django import mixer

from sandtex.main.models import Bid, BidItem

# from applications.reputation.models import ReputationCase
# from applications.search.models import UserSearch
# from applications.social_sites.models import SocialProfile
# from applications.social_sites.utils import get_random_weight

# from applications.accounting.models import Order
# from applications.profiles.models import User

pytestmark = pytest.mark.django_db


#
# @pytest.mark.skip(reason="Moved to the Django Allauth lib")
def test_open_dashboard_with_test_user(client):
    pass
