import pytest
from django.core.urlresolvers import reverse
from mixer.backend.django import mixer

from sandtex.main.models import Bid, BidItem
from .fixtures import test_user

test_user
# from applications.reputation.models import ReputationCase
# from applications.search.models import UserSearch
# from applications.social_sites.models import SocialProfile
# from applications.social_sites.utils import get_random_weight

# from applications.accounting.models import Order
# from applications.profiles.models import User

pytestmark = pytest.mark.django_db


#
# @pytest.mark.skip(reason="Moved to the Django Allauth lib")
def test_open_dashboard_with_test_user(client, test_user):
    logged_in = client.login(username=test_user.username,
                             password=test_user.username)

    test_bid = mixer.blend(Bid, group_id=1, is_open=True, group=None, customer=None, author=test_user,
                           modified=None)

    assert logged_in is True, "User did not login"

    response = client.get(reverse("dashboard"))
    assert response.status_code == 200, "Cant view dashboard"

    response = client.get(reverse('bid-create'))
    assert response.status_code == 200, "Cant get bid-create page"

    response = client.get(reverse('bids'))
    assert test_bid in response.context['bids'], "Cant find just created bid in bids list"

    response = client.get(test_bid.get_absolute_url())
    assert response.status_code == 200, "Cant get bid-update page"

    # create bid_item
    bid_item = mixer.blend(BidItem, bid=test_bid, authod=test_user, modified=None)

    response = client.get(test_bid.get_rfq_url())
    assert response.status_code == 200, "Cant get RFQ-create page"

    # view history
    response = client.get(test_bid.get_history_url())
    assert response.status_code == 200, "Cant get History page"

    assert len(response.context['bid'].get_full_history()) > 1, 'too small history records in history'



# @pytest.mark.skip(reason="Moved to the Django Allauth lib")
def test_open_dashboard_unlogin(client):
    response = client.get(reverse("dashboard"))
    assert response.status_code == 302, "Dashboard should redirect to login"

# bid-create - get, post
# bids - find in list
# bids-update - open
# item-create - post item
# rfq-create - get
# history - check page
