import pytest
from mixer.backend.django import mixer

from sandtex.users.models import User, Group


@pytest.fixture
def test_user():
    test_group = mixer.blend(Group)
    user = mixer.blend(User, username="test_user", grou=test_group)
    user.set_password("test_user")
    user.save()
    return user
