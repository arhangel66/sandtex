import os
from itertools import chain

from django.core.wsgi import get_wsgi_application

os.environ['DJANGO_SETTINGS_MODULE'] = 'config.settings.local'
application = get_wsgi_application()
from sandtex.main.models import Bid, BidItem, Rfq


# from sandtex.main.models import Bid
# from django.db.models import Q
# get_vendors_list()

# from os import walk

# from sandtex.main.views import DashboardView
# DashboardView().get_context_data()
def get_changes_from_previous_version(o1, o2):
    result = {}
    # get list of all fields
    fields = list(set(chain.from_iterable(
        (field.name, field.attname) if hasattr(field, 'attname') else (field.name,)
        for field in o1._meta.get_fields()
        if not (field.many_to_one and field.related_model is None)
    )))

    for field in fields:
        if getattr(o1, field, None) != getattr(o2, field, None):
            result[field] = getattr(o1, field, None)
            # result += 'field {} changed to "{}"\n'.format(field, )
    return result


prev = None


def get_full_bid_history(bid):
    list_of_hist = []
    list_of_hist += list(bid.history.all())
    # bid_item
    for bid_item in BidItem.objects.filter(bid=bid):
        list_of_hist += list(bid_item.history.all())

    # RFQ
    for rfq in Rfq.objects.filter(bid=bid):
        list_of_hist += list(rfq.history.all())

    # sort
    # print(list_of_hist)
    list_of_hist = sorted(list_of_hist, key=lambda line: line.history_date)

    # add prev_changes
    prev = {}
    for line in list_of_hist:
        line.changed = ''
        model_name = line.instance._meta.verbose_name
        key = "{}_{}".format(model_name, line.instance.id)
        if line.get_history_type_display() == 'Changed' and prev.get(key):
            line.changed = get_changes_from_previous_version(line.instance, prev[key].instance)
        prev[key] = line

    return list_of_hist


def show_list_of_hist(list_of_hist):
    for line in list_of_hist:
        model_name = line.instance._meta.verbose_name
        print(" | ".join(
            [str(line.history_date), line.get_history_type_display(), model_name, str(line.instance),
             str(line.instance.id), str(line.changed)]))


bid = Bid.objects.get(id=3)
list_of_hist = get_full_bid_history(bid)
show_list_of_hist(list_of_hist)

# for h in hist:
# print(h)
# if prev:
# print(get_changes_from_previous_version(h.history_object, prev.history_object))
# prev = h
# print(attr, type(getattr(bids[0], attr)))
# print(bids[0].get_deferred_fields())


"""
__class__ <class 'django.db.models.base.ModelBase'>
__delattr__ <class 'method-wrapper'>
__dict__ <class 'dict'>
__dir__ <class 'builtin_function_or_method'>
__doc__ <class 'str'>
__eq__ <class 'method'>
__format__ <class 'builtin_function_or_method'>
__ge__ <class 'method-wrapper'>
__getattribute__ <class 'method-wrapper'>
__gt__ <class 'method-wrapper'>
__hash__ <class 'method'>
__init__ <class 'method'>
__init_subclass__ <class 'builtin_function_or_method'>
__le__ <class 'method-wrapper'>
__lt__ <class 'method-wrapper'>
__module__ <class 'str'>
__ne__ <class 'method'>
__new__ <class 'builtin_function_or_method'>
__reduce__ <class 'method'>
__reduce_ex__ <class 'builtin_function_or_method'>
__repr__ <class 'method'>
__setattr__ <class 'method-wrapper'>
__setstate__ <class 'method'>
__sizeof__ <class 'builtin_function_or_method'>
__str__ <class 'method'>
__subclasshook__ <class 'builtin_function_or_method'>
__weakref__ <class 'NoneType'>
_base_manager <class 'django.db.models.manager.Manager'>
_check_column_name_clashes <class 'method'>
_check_field_name_clashes <class 'method'>
_check_fields <class 'method'>
_check_id_field <class 'method'>
_check_index_together <class 'method'>
_check_local_fields <class 'method'>
_check_long_column_names <class 'method'>
_check_m2m_through_same_relationship <class 'method'>
_check_managers <class 'method'>
_check_model <class 'method'>
_check_ordering <class 'method'>
_check_swappable <class 'method'>
_check_unique_together <class 'method'>
_default_manager <class 'django.db.models.manager.Manager'>
_deferred <class 'bool'>
_do_insert <class 'method'>
_do_update <class 'method'>
_get_FIELD_display <class 'method'>
_get_next_or_previous_by_FIELD <class 'method'>
_get_next_or_previous_in_order <class 'method'>
_get_pk_val <class 'method'>
_get_unique_checks <class 'method'>
_meta <class 'django.db.models.options.Options'>
_perform_date_checks <class 'method'>
_perform_unique_checks <class 'method'>
_save_parents <class 'method'>
_save_table <class 'method'>
_set_pk_val <class 'method'>
_state <class 'django.db.models.base.ModelState'>
author <class 'sandtex.users.models.User'>
author_id <class 'int'>
check <class 'method'>
clean <class 'method'>
clean_fields <class 'method'>
close_dt <class 'NoneType'>
created <class 'datetime.datetime'>
customer <class 'NoneType'>
customer_id <class 'NoneType'>
date_error_message <class 'method'>
delete <class 'method'>
due_date <class 'datetime.date'>
from_db <class 'method'>
full_clean <class 'method'>
get_deferred_fields <class 'method'>
get_history_type_display <class 'method'>
get_next_by_created <class 'method'>
get_next_by_history_date <class 'method'>
get_next_by_modified <class 'method'>
get_previous_by_created <class 'method'>
get_previous_by_history_date <class 'method'>
get_previous_by_modified <class 'method'>
group <class 'sandtex.users.models.Group'>
group_id <class 'int'>
history_change_reason <class 'NoneType'>
history_date <class 'datetime.datetime'>
history_id <class 'int'>
history_object <class 'sandtex.main.models.Bid'>
history_type <class 'str'>
history_user <class 'sandtex.users.models.User'>
history_user_id <class 'int'>
id <class 'int'>
instance <class 'sandtex.main.models.Bid'>
instance_type <class 'django.db.models.base.ModelBase'>
is_open <class 'bool'>
modified <class 'datetime.datetime'>
name <class 'str'>
notes <class 'str'>
"""




# x = [12, 34, 29, 38, 34, 51, 29, 34, 47, 34, 55, 94, 68, 81]
# a,b = linreg(range(len(x)),x)
#
# for i in range(len(x)):
#     print(i, x[i], a*i+b)
# from sandtex.main.models import get_customer_list
# get_customer_list()



# 1. Get all bids, who was open  more than 30 days ago and was closed less than 30 days ago



