import datetime
from django import template
from django.template.loader import get_template

from sandtex.users.models import Group

register = template.Library()


@register.simple_tag
def show_groups(current, user_group):
    # template = ""
    t = get_template('blocks/show_groups.html')
    html = t.render({'groups': Group.objects.all(), 'cur_group': current, 'user_group': user_group})
    # for group in .values():
    #     template += "<option value='%s'>%s</option>" % (group['id'], group['name'])
    return html
