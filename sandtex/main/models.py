import datetime
import glob
import json
import os

from allauth.compat import render_to_string
from django.core.mail import EmailMultiAlternatives
from django.core.urlresolvers import reverse
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.models import TimeStampedModel
from simple_history.models import HistoricalRecords

from sandtex.main.files.utils import my_update_change_reason, file_renamed
from sandtex.users.models import Group, User


class FileNameListMixin(models.Model):
    class Meta:
        abstract = True

    file_names = models.TextField(default='[]')

    def get_filenames_from_db(self):
        return json.loads(self.file_names)

    def set_filenames(self, file_list):
        self.file_names = json.dumps(file_list)
        self.save()

    def update_filename(self, old_name, new_name, history=False):
        self.file_names = self.file_names.replace('"{}"'.format(old_name), '"{}"'.format(new_name))
        if history:
            self.save()
        else:
            self.save_without_historical_record()


class Bid(FileNameListMixin, TimeStampedModel):
    name = models.CharField(_('Bid name'), max_length=100, blank=True, null=True, default='')
    due_date = models.DateField(_('Due date of bid'), blank=True, null=True)
    author = models.ForeignKey(User, verbose_name=_('Authour'), blank=True, null=True)
    group = models.ForeignKey(Group, blank=True, null=True)
    notes = models.TextField(_('Notes'), blank=True, null=True)
    is_open = models.BooleanField(default=True)
    close_dt = models.DateTimeField(_('Close datetime'), blank=True, null=True, default=None)
    # client = models.CharField(_('client'), max_length=150)  # Todo make lib for clients
    customer = models.ForeignKey('main.Customer', blank=True, null=True, default=None)
    history = HistoricalRecords(excluded_fields=['modified'])


    def __str__(self):
        if len(self.name) > 0:
            return "%s" % self.name
        return "Unnamed bid N%s" % self.id

    def get_html(self):
        return self.__str__()

    def get_absolute_url(self):
        return reverse('bid-update', kwargs={'pk': self.id})

    def get_rfq_url(self):
        return reverse('rfq-create', kwargs={'bid_pk': self.id})

    def get_history_url(self):
        return reverse('bid-history', kwargs={'pk': self.id})

    def get_items(self):
        return BidItem.objects.filter(bid=self.id).order_by('id')

    def get_directory_path(self):
        from django.conf import settings
        path = os.path.abspath(os.path.join(settings.MEDIA_ROOT, self.author.username, str(self.id)))
        return path

    def get_file_path(self, filename):
        from django.conf import settings
        path = os.path.abspath(os.path.join(settings.MEDIA_ROOT, self.author.username, str(self.id), filename))
        print(path)
        return path

    def close(self):
        if self.is_open:
            self.is_open = False
            self.close_dt = datetime.datetime.now()
            self.save()
        return True

    def get_full_history(self):
        from sandtex.history.utils import get_full_bid_history
        return get_full_bid_history(self)

    def get_close_url(self):
        return reverse('bid-close', kwargs={'pk': self.id})

    def get_filename_list(self):
        full_files = glob.glob("{}/*.*".format(self.get_directory_path()))
        filenames = [os.path.basename(filepath) for filepath in full_files]
        return filenames

    def compare_files(self):
        db = self.get_filenames_from_db()
        disk = self.get_filename_list()
        # print(100, db, disk)
        if set(db) != set(disk) and len(disk) + len(db) > 0:
            print(set(db), set(disk))
            if len(disk) > len(db):
                reason ='add file - {}'.format(set(disk) - set(db))
                self.set_filenames(self.get_filename_list())
                my_update_change_reason(self, reason)
            elif len(disk) < len(db):
                reason = 'del file - {}'.format(set(db) - set(disk))
                self.set_filenames(self.get_filename_list())
                my_update_change_reason(self, reason)
            else:
                # reason = 'rename file - {} -> {}'.format(set(db) - set(disk), set(disk) - set(db))
                old_name = list(set(db) - set(disk))[0]
                new_name = list(set(disk) - set(db))[0]
                file_renamed(old_name, new_name, bid_pk=self.id)
        else:
            print('all is good')


class BidItem(TimeStampedModel):
    bid = models.ForeignKey(Bid, verbose_name=_('Bid'), blank=True, null=True)
    qty = models.IntegerField(_('quantity'), blank=True, null=True)
    descr_span = models.CharField(_('description in spanish'), blank=True, null=True, default='', max_length=200)
    brand = models.CharField(_('brand'), blank=True, null=True, max_length=100)  # Todo make lib for brands
    descr_eng = models.CharField(_('description in english'), blank=True, null=True, default='', max_length=200)
    is_open = models.BooleanField(default=True)
    author = models.ForeignKey(User, blank=True, null=True)
    weight = models.CharField(_('Weight'), blank=True, null=True, max_length=50)
    lead_time = models.CharField(_('Lead Time'), blank=True, null=True, max_length=50)
    notes = models.TextField(_('Notes'), blank=True, null=True)
    best_quote = models.CharField(_('Best Quote'), blank=True, null=True, max_length=200)

    history = HistoricalRecords(excluded_fields=['modified'])

    def get_name(self):
        if self.descr_eng:
            return self.descr_eng
        return self.descr_span

    def get_short_name(self):
        name = self.get_name()
        return name[:30]+'..'

    def __str__(self):
        return "%s, brand: %s" % (self.get_short_name(), self.brand)
        # rfq = Rfq.objects.filter(bid=self.bid_id, items=self.id).values_list('vendor__name', flat=True)
        # if not rfq:
        #     return "%s - %s - no rfq" % (self.get_name(), self.brand)
        # else:
        #     return "%s - %s - (%s)" % (self.get_name(), self.brand, ",".join(rfq))

    def get_html(self):
        return "%s <br>brand: %s" % (self.get_short_name(), self.brand)

    def get_bid_url(self):
        return reverse('bid-update', kwargs={'pk': self.bid.id})

    def get_delete_url(self):
        return reverse('item-delete', kwargs={'bid_pk': self.bid_id, 'pk': self.id})

    def get_update_url(self):
        return reverse('item-update', kwargs={'bid_pk': self.bid_id, 'pk': self.id})


class Vendor(TimeStampedModel):
    name = models.CharField(_('Name'), blank=True, null=True, max_length=100)
    rating = models.IntegerField(_('Rating'), blank=True, null=True)
    email = models.CharField(_('Email'), blank=True, null=True, max_length=100)

    def __str__(self):
        return "%s - %s" % (self.name, self.email)

    def get_update_url(self):
        return reverse('vendor-update', kwargs={'pk': self.id})


def get_upload_path(instance, filename):
    return os.path.join("bid_%d" % instance.bid.id, 'rfq-' + filename)


class Rfq(FileNameListMixin, TimeStampedModel):
    bid = models.ForeignKey(Bid, blank=True, null=True)
    vendor = models.ForeignKey(Vendor, blank=True, null=True)
    gen_file = models.FileField(verbose_name="/", upload_to=get_upload_path)
    items = models.ManyToManyField(BidItem, blank=True, null=True)
    email = models.CharField(_('email'), blank=True, null=True, max_length=150)
    is_send = models.BooleanField(_('is_send'), default=False)
    send_dt = models.DateTimeField(_('send datetime'), blank=True, null=True)
    author = models.ForeignKey(User, blank=True, null=True)

    history = HistoricalRecords(excluded_fields=['modified'])

    # class Meta:
    #     unique_together = ('bid', 'vendor',)

    def __str__(self):
        return "vendor: %s, items: %s" % (self.vendor.name, self.get_list_items_names())
        # return "%s-%s" % (self.bid, self.vendor)

    def is_vendor_email_good(self):
        return self.vendor and '@' in self.vendor.email

    def compare_files(self):
        self.bid.compare_files()

    def get_html(self):
        return """
        vendor: %s<br>
        items: %s
        """ % (self.vendor.name, self.get_list_items_names())

    def get_list_items_names(self):
        return [item.get_short_name() for item in self.items.all()]

    def get_delete_url(self):
        return reverse('rfq-delete', kwargs={'bid_pk': self.bid_id, 'pk': self.id})

    def get_absolute_url(self):
        return reverse('rfq-create', kwargs={'bid_pk': self.bid_id})

    def get_pdf_url(self):
        return reverse('rfq-pdf', kwargs={'bid_pk': self.bid_id, 'rfq_pk': self.id})

    def get_pdf_download_url(self):
        return reverse('rfq-pdf-download', kwargs={'bid_pk': self.bid_id, 'rfq_pk': self.id})

    def get_send_url(self):
        return reverse('rfq-send', kwargs={'bid_pk': self.bid_id, 'rfq_pk': self.id})

    def get_list_of_filepaths(self):
        paths = []
        for file in self.get_filenames_from_db():
            paths.append(self.bid.get_file_path(file))
        return paths

    @property
    def number(self):
        return "1{0:05d}{0:03d}".format(self.id, self.vendor_id)

    def get_eml_path(self):
        return os.path.join(self.bid.get_directory_path(), self.number)+'.eml'

    def get_email_html(self):
        context = {
            "bid": self.bid,
            'rfq': self
        }
        html_message = render_to_string('pages/pdf.html', context)
        # html_message = render_to_string('email/rfq.html', context)
        return html_message

    def send_email(self, force=False):
        if force or not self.is_send:
            from django.core.mail import EmailMessage
            message = EmailMessage('Sandtex: New Request for Pricing', 'Request the price', 'admin@sandtex.com',
                                   ['arhangel662@gmail.com'],
                                   )
            from easy_pdf.rendering import render_to_pdf
            context = {
                "bid": self.bid,
                'rfq': self
            }
            pdf = render_to_pdf("pages/pdf.html", context, using=None)
            # message.attach(self.get_filename(), pdf, 'application/pdf')
            message.send()

            self.is_send = True
            self.save()
            return True


    def get_from_email(self):
        if 'sandtexusa' in self.author.email:
            return self.author.email
        else:
            return "robot@sandtexusa.com"

    def send_html_email(self, force=False, to=None):
        from sandtex.main.files.save_eml import Gen_Emails
        if force or not self.is_send:
            if not to:
                to = self.vendor.email
            subject = 'Sandtex: New Request for Pricing'
            message = EmailMultiAlternatives(subject,
                                             'Request the price',
                                             self.get_from_email(),
                                             [to],
                                             )
            message.attach_alternative(self.get_email_html(), "text/html")
            for file_path in self.get_list_of_filepaths():
                print('attaching {}'.format(file_path))
                message.attach_file(file_path)

            message.send()

            # save eml

            Gen_Emails().EmailGen(rfq_id=self.id,
                                  sender=self.get_from_email(),
                                  recepiant=to,
                                  subject=subject,
                                  html=self.get_email_html(),
                                  files=self.get_list_of_filepaths())

            self.is_send = True
            self.save()
            # send_mail(
            #     'Sandtex: New Request for Pricing',
            #     'text_message',
            #     'robot@sandtexusa.com',
            #     [to],
            #     fail_silently=False,
            #     html_message=html_message
            # )

    def get_filename(self):
        return "RFQ:{}-{}.pdf".format(self.vendor.id, self.id)

    def is_urgent(self):
        if self.bid.due_date and (self.bid.due_date - datetime.date.today()).days < 2:
            return (self.bid.due_date - datetime.date.today()).days
        return False



class Customer(TimeStampedModel):
    name = models.CharField(max_length=20, blank=True, null=True)
    notes = models.CharField(max_length=250, blank=True, null=True)
    email = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
        return "%s - %s" % (self.name, self.id)

    class Meta:
        ordering = ('name',)


# class File


def get_vendors_list():
    from sandtex.olddb.models import VdrInfo
    vdrs = VdrInfo.objects.all().values('vdr_id', 'e1')
    our_vdr = Vendor.objects.all().values_list('name', flat=True)

    count_vendors = 0
    for vdr in vdrs:
        if vdr['vdr_id'] not in our_vdr:
            count_vendors += 1
            Vendor.objects.create(name=vdr['vdr_id'], email=vdr['e1'])
    print("%s vendors added" % count_vendors)


def get_customer_list():
    from sandtex.olddb.models import Tblsalesorders
    customers = Tblsalesorders.objects.all().values_list('customer', flat=True).distinct()
    our_customers = Customer.objects.all().values_list('name', flat=True)
    new_customers = set(customers) - set(our_customers)

    for customer in new_customers:
        Customer.objects.create(name=customer)
    count_customers = len(new_customers)

    print("%s customers added" % count_customers)
