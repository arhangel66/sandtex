from django.core.management.base import BaseCommand


class Command(BaseCommand):
    def handle(self, *args, **options):
        from sandtex.main.models import get_customer_list, get_vendors_list
        get_customer_list()
        get_vendors_list()
