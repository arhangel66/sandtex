import simple_history
from django.contrib import admin

from .models import *


@admin.register(Bid)
class BidAdmin(simple_history.admin.SimpleHistoryAdmin):
    list_display = ('name', 'customer', 'notes', 'due_date', 'author', 'group', 'created', 'is_open', 'close_dt')
    list_filter = ('author', 'group',)
    # form = MyUserChangeForm
    # add_form = MyUserCreationForm


@admin.register(BidItem)
class BidItemAdmin(admin.ModelAdmin):
    list_display = ('get_name', 'brand', 'qty', 'bid')


@admin.register(Rfq)
class RfqAdmin(admin.ModelAdmin):
    list_display = ('bid', 'vendor', 'is_send')
    list_filter = ('vendor',)


@admin.register(Vendor)
class VendorAdmin(admin.ModelAdmin):
    list_display = ('name', 'email', 'rating')
    list_editable = ('rating', 'email')
    search_fields = ('name', )


@admin.register(Customer)
class CustomerAdmin(admin.ModelAdmin):
    list_display = ('name', 'notes', 'email')
    list_editable = ('notes', 'email')
    search_fields = ('name', )
