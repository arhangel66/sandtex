from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, HTML
from django import forms
from django.core.urlresolvers import reverse
import os

from sandtex.main.files.utils import rename_file
from sandtex.main.models import Bid, BidItem


class EditFileNameForm(forms.Form):
    old_name = forms.CharField(widget=forms.HiddenInput())
    new_name = forms.SlugField(widget=forms.TextInput(attrs={'pattern': '[-a-zA-Z0-9_]{2,}'}))

    def __init__(self, *args, **kwargs):
        bid_id = kwargs.pop('bid_pk')
        ext = kwargs.pop('ext')
        super().__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_action = reverse('edit_file', kwargs={'bid_pk': bid_id})
        self.helper.form_class = 'form-inline'
        self.helper.field_template = 'bootstrap3/layout/inline_field.html'
        self.helper.layout = Layout(
            'old_name',
            'new_name',
            HTML("""
                    
                    <div class="form-group">{}</div><input type="submit" name="submit" value="Rename" class="btn btn-primary" id="submit-id-submit" style="visibility:hidden">
                """.format(ext)),
        )

    def save(self, bid_pk):
        from django.conf import settings
        cd = self.cleaned_data

        media_path = settings.MEDIA_ROOT
        oldpath = cd['old_name'].replace('/media', media_path)
        rename_file(full_old_path=oldpath, new_filename=cd['new_name'], bid_pk=bid_pk)
        return True

