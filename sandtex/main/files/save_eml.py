from email import generator
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import formatdate
from os.path import basename

active_dir = '/'

import os

cwd = os.getcwd()
# outfile_name = os.path.join(cwd, 'message.eml')


class Gen_Emails(object):
    # def __init__(self):
        # self.EmailGen()

    def EmailGen(self, rfq_id, sender='', recepiant='', subject='', html='',
                 files=['/Users/mikhail/w/sandtex/sandtex/sandtex/main/files/utils.py']):
        msg = MIMEMultipart('alternative')
        msg['Subject'] = subject
        msg['From'] = sender
        msg['To'] = recepiant
        msg['Date'] = formatdate(localtime=True)

        part = MIMEText(html, 'html')

        msg.attach(part)

        # part['Content-Disposition'] = 'attachment; filename="%s"' % basename(f)
        # msg.attach(part)

        for f in files or []:
            with open(f, "rb") as fil:
                part = MIMEApplication(
                    fil.read(),
                    Name=basename(f)
                )
            # After the file is closed
            part['Content-Disposition'] = 'attachment; filename="%s"' % basename(f)
            msg.attach(part)
        from sandtex.main.models import Rfq
        rfq = Rfq.objects.get(id=rfq_id)
        self.SaveToFile(msg, outfile_name=rfq.get_eml_path())

    def SaveToFile(self, msg, outfile_name):
        with open(outfile_name, 'w') as outfile:
            gen = generator.Generator(outfile)
            gen.flatten(msg)



