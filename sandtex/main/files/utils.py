import os
import subprocess
import sys


def open_file(filename):
    if sys.platform == "win32":
        os.startfile(filename)
    else:
        opener ="open" if sys.platform == "darwin" else "xdg-open"
        subprocess.call([opener, filename])


def rename_file(full_old_path, new_filename, bid_pk):
    old_name = os.path.basename(full_old_path)

    # get directory and ext
    file_dir = os.path.dirname(full_old_path)
    ext = os.path.splitext(os.path.basename(full_old_path))[1]
    new_name = new_filename + ext
    new_full_path = os.path.join(file_dir, new_name)
    os.rename(full_old_path, new_full_path)
    file_renamed(old_name, new_name, bid_pk=bid_pk)


def file_renamed(old_name, new_name, bid_pk):
    """
    change filename in BidItem and RFQ
    :param old_name:
    :param new_name:
    :param bid_pk:
    :return:
    """
    from sandtex.main.models import BidItem, Rfq, Bid

    BidItem.objects.filter(bid=bid_pk, best_quote=old_name).update(best_quote=new_name)
    rfqs = Rfq.objects.filter(bid=bid_pk, file_names__contains=old_name)
    for rfq in rfqs:
        rfq.update_filename(old_name, new_name)
    bid = Bid.objects.get(id=bid_pk)
    bid.update_filename(old_name, new_name, history=True)
    my_update_change_reason(bid, 'rename file - {} -> {}'.format(old_name, new_name))
    return True



def my_update_change_reason(instance, reason):
    attrs = {}
    model = type(instance)
    manager = instance if instance.id is not None else model
    for field in instance._meta.fields:
        if field.attname != 'modified':
            value = getattr(instance, field.attname)
            if field.primary_key is True:
                if value is not None:
                    attrs[field.attname] = value
            else:
                attrs[field.attname] = value
    record = manager.history.filter(**attrs).order_by('-history_date').first()
    if record:
        record.history_change_reason = reason
        record.save()
