import os
from os.path import basename

from braces.views import LoginRequiredMixin, CsrfExemptMixin, JSONResponseMixin
from django.core.files.storage import FileSystemStorage
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.shortcuts import redirect
from django.views.generic import FormView, TemplateView
from django.views.generic import View

from sandtex.main.files.extract_eml import extract
from sandtex.main.files.utils import open_file
from sandtex.main.models import Bid
from . import forms


class DeleteFile(CsrfExemptMixin, View):
    success_url = '/'

    def post(self, request, *args, **kwargs):
        return HttpResponse('Ok')
        # if self.get_object().user == self.request.user:
        #     super(DeleteFile, self).post(request, *args, **kwargs)
        #     return HttpResponse('Ok')
        # return HttpResponse('No')



class EditFile(FormView):
    template_name = 'blocks/blank-form.html'
    form_class = forms.EditFileNameForm

    def get_initial(self):
        init = super().get_initial()
        filename = self.request.GET.get('filename')
        if filename:
            fname = os.path.splitext(basename(filename))[0]
            init['old_name'] = filename
            init['new_name'] = fname

        return init

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['bid_pk'] = self.kwargs.get('bid_pk')
        if self.request.GET.get('filename'):
            kwargs['ext'] = os.path.splitext(basename(self.request.GET.get('filename')))[1]
        else:
            kwargs['ext'] = ''
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['bid_pk'] = self.kwargs.get('bid_pk')
        return context

    def form_valid(self, form):
        form.save(self.kwargs.get('bid_pk'))
        return redirect(reverse('bid-update', kwargs={'pk': self.kwargs.get('bid_pk')}))



class ViewFile(CsrfExemptMixin, View):
    def post(self, request, bid_pk):
        filename = request.POST.get('file')
        if request.POST.get('folder'):
            open_file(request.POST.get('folder'))
        bid = Bid.objects.get(id=bid_pk)
        full_filename = bid.get_file_path(basename(filename))
        open_file(full_filename)
        return HttpResponse('Ok')


class GetFiles(LoginRequiredMixin, JSONResponseMixin, View):
    def get(self, request, *args, **kwargs):
        # print(kwargs)
        return self.render_json_response(context_dict=self.get_data(), status=200)

    def get_data(self):
        # files = UploadedFiles.objects.filter(user=self.request.user)
        from os import walk
        from django.conf import settings

        bid = Bid.objects.get(id=self.kwargs.get('bid_pk'))
        mypath = bid.get_directory_path()

        files = []
        files_list = []
        for (dirpath, dirnames, filenames) in walk(mypath):
            files.extend(filenames)
            break
        for fil in files:
            url = mypath.replace('\\', '/').replace(settings.MEDIA_ROOT, '/media/') + '/' + fil
            try:
                # razr = fil.split('.')[-1]
                # if razr.lower() in ['jpg', 'pdf', 'png', 'bpm', 'eml']:
                files_list.append(
                    {'name': fil, 'size': os.path.getsize(mypath + '/' + fil), 'good': 1, 'serverId': fil, 'url': url})
                continue
            except:
                pass
            # files_list.append(
            #     {'name': fil, 'size': os.path.getsize(mypath + '/' + fil), 'serverId': fil, 'url': url})


        return files_list


def handle_uploaded_file(file, username, bid_id=1):
    bid = Bid.objects.get(id=bid_id)
    directory = bid.get_directory_path()
    # directory = os.path.abspath(os.path.join(settings.MEDIA_ROOT, username, bid_id))
    storage = FileSystemStorage(location=directory)
    # file.name = file_name_obrab(file.name)

    # Check to see if Users folder already exists
    if not storage.exists(directory):
        os.makedirs(directory)
        os.chmod(directory, 0o777)
        print()
    # Use djangos FileSystemStorage to see if file exists will rename accordingly
    if storage.exists(file.name):
        file.name = storage.get_available_name(file.name)
    with open(os.path.abspath(os.path.join(directory, file.name)), 'wb+') as destination:
        for chunk in file.chunks():
            destination.write(chunk)
    return os.path.abspath(os.path.join(directory, file.name))


class PreViewFile(CsrfExemptMixin, TemplateView):
    template_name = 'blocks/eml.html'

    def post(self, request, bid_pk):
        return self.get(request, bid_pk=bid_pk)

    def get_file_type(self, filename):
        razr = filename.split('.')[-1].lower()
        if razr in ['jpg', 'pdf', 'png', 'bpm']:
            type = 'iframe'
        elif razr == 'eml':
            type = 'eml'
        else:
            type = 'other'
        return type

    def get(self, request, *args, **kwargs):
        # context = super(PreViewFile, self).get_context_data(**kwargs)
        filename = self.request.POST.get('file')
        file_type = self.get_file_type(filename)
        if file_type == 'iframe':
            return HttpResponse('<iframe src="{}" width="600" height="600"></iframe>'.format(filename))
        elif file_type == 'eml':
            print(335, kwargs)
            return self.eml_template(**kwargs)
        return "No preview for this type of file"
        # bid = Bid.objects.get(id=bid_pk)
        # full_filename = bid.get_file_path(basename(filename))

        # data['html'] = data['html'].replace('=\n', '').replace('=0A', '')
        # context.update(data)

    def eml_template(self, **kwargs):
        print(345, kwargs)
        context = self.get_context_data(**kwargs)
        bid = Bid.objects.get(id=kwargs.get('bid_pk'))
        full_filename = bid.get_file_path(basename(self.request.POST.get('file')))
        with open(full_filename, 'r') as f:
            data = extract(f, f.name)
            print(349, data)
            data['html'] = data['html'].replace('=\n', '').replace('=0A', '')
        context.update(data)
        return self.render_to_response(context)
