from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit
from django import forms
from django.core.urlresolvers import reverse

from sandtex.main import models
from sandtex.main.models import Vendor, BidItem, Rfq, Bid


class BidCreateForm(forms.ModelForm):
    class Meta:
        model = models.Bid
        fields = ('name', 'due_date', 'notes', 'customer')
        widgets = {
            'customer': forms.Select(attrs={'class': 'select2_single form-control'})
        }


class ItemCreateForm(forms.ModelForm):
    class Meta:
        model = models.BidItem
        fields = ('qty', 'brand', 'descr_eng', 'descr_span')


class ItemUpdateForm(forms.ModelForm):
    best_quote = forms.ChoiceField(required=False, widget=forms.Select( attrs={'class': 'select2'}))

    class Meta:
        model = models.BidItem
        fields = ('qty', 'brand', 'descr_eng', 'descr_span', 'weight', 'lead_time', 'notes', 'is_open')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        item = kwargs.get('instance')
        bid = Bid.objects.get(id=item.bid_id)
        filenames = [(filename, filename) for filename in bid.get_filename_list()]
        self.fields['best_quote'].choices = [('', '-----------')] + filenames
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_action = reverse('item-update', kwargs={'bid_pk': item.bid_id, 'pk': item.id})
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-lg-2'
        self.helper.field_class = 'col-lg-9'
        self.helper.layout = Layout(
            'brand',
            'qty',
            'descr_eng',
            'descr_span',
            'weight', 'lead_time', 'best_quote', 'notes',
            'is_open',
        )


class RfqCreateForm(forms.Form):
    vendors = forms.ModelMultipleChoiceField(label="Vendor", queryset=Vendor.objects.all(),
                                             widget=forms.SelectMultiple(attrs={'class': 'select2_multiple', }))
    items = forms.ModelMultipleChoiceField(label="Items",
                                           widget=forms.SelectMultiple(attrs={'class': 'select2_multiple', }),
                                           required=False, queryset=BidItem.objects.all())
    files = forms.MultipleChoiceField(label="files appended to mail", required=False,
                                      widget=forms.SelectMultiple(attrs={'class': 'select2_multiple'}))

    class Meta:
        model = models.BidItem
        fields = ('qty', 'brand', 'descr_eng', 'descr_span', 'weight', 'lead_time', 'notes', 'is_open')


    def __init__(self, *args, **kwargs):
        bid_id = kwargs.pop('bid_pk')
        super().__init__(*args, **kwargs)
        self.fields['items'].queryset = BidItem.objects.filter(bid=bid_id)
        bid = Bid.objects.get(id=bid_id)

        filenames = [(filename, filename) for filename in bid.get_filename_list()]
        self.fields['files'].choices = filenames

        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_action = reverse('rfq-create', kwargs={'bid_pk': bid_id})
        self.helper.form_class = 'form-inline'
        self.helper.field_template = 'bootstrap3/layout/inline_field.html'
        self.helper.layout = Layout(
            'items',
            'vendors',
            'files',
            Submit('submit', 'Create RFQ'),
        )

    def save(self, bid_pk, author):
        cd = self.cleaned_data
        if cd.get('items'):
            for vendor in cd['vendors']:
                rfq, is_created = Rfq.objects.get_or_create(bid_id=bid_pk, vendor=vendor, is_send=False, author=author)
                rfq_items = rfq.items.all().values_list('id', flat=True)
                for item in cd.get('items'):
                    if not item.id in rfq_items:
                        rfq.items.add(item)
                rfq.save()
                rfq.set_filenames(cd.get('files'))


class CustomerCreateForm(forms.ModelForm):
    class Meta:
        model = models.Customer
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_action = reverse('customer-create')
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-lg-2'
        self.helper.field_class = 'col-lg-5'
        self.helper.layout = Layout(
            'name',
            'notes',
            'email'
        )


class VendorCreateForm(forms.ModelForm):

    class Meta:
        model = models.Vendor
        fields = "__all__"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_action = reverse('vendor-create')
        self.helper.form_class = 'form-horizonal'
        self.helper.label_class = 'col-lg-3'
        self.helper.field_class = 'col-lg-8'
        self.helper.layout = Layout(
            'name',
            'rating',
            'email'
        )


class VendorUpdateForm(VendorCreateForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        vendor = kwargs.get('instance')
        self.helper.form_action = reverse('vendor-update', kwargs={'pk': vendor.id})
