from django.views.generic import TemplateView, DetailView

from sandtex.main.models import Bid
from sandtex.main.views import UpdateFilesMixin


class BidHistoryView(UpdateFilesMixin, DetailView):
    template_name = 'pages/history/bid.html'
    model = Bid
    context_object_name = 'bid'
