from itertools import chain

from sandtex.main.models import BidItem, Rfq


def get_changes_from_previous_version(o1, o2):
    result = {}
    # get list of all fields
    fields = list(set(chain.from_iterable(
        (field.name, field.attname) if hasattr(field, 'attname') else (field.name,)
        for field in o1._meta.get_fields()
        if not (field.many_to_one and field.related_model is None)
    )))

    for field in fields:
        if getattr(o1, field, None) != getattr(o2, field, None):
            result[field] = getattr(o1, field, None)
            # result += 'field {} changed to "{}"\n'.format(field, )
    return result


def get_full_bid_history(bid):
    list_of_hist = []
    list_of_hist += list(bid.history.all())
    # bid_item
    for bid_item in BidItem.objects.filter(bid=bid):
        list_of_hist += list(bid_item.history.all())

    # RFQ
    for rfq in Rfq.objects.filter(bid=bid):
        list_of_hist += list(rfq.history.all())

    # sort
    list_of_hist = sorted(list_of_hist, key=lambda line: line.history_date)

    # add prev_changes
    prev = {}
    for line in list_of_hist:
        line.changed = ''
        line.model_name = line.instance._meta.verbose_name
        key = "{}_{}".format(line.model_name, line.instance.id)
        if line.get_history_type_display() == 'Changed' and prev.get(key):
            line.changed = get_changes_from_previous_version(line.instance, prev[key].instance)
        prev[key] = line

    return list_of_hist
