from django.contrib import admin

from sandtex.olddb.models import *

@admin.register(Tblsalesummaries)
class TblsalesummariesAdmin(admin.ModelAdmin):
    list_display = ('id', 'generateddate')
#
@admin.register(VdrInfo)
class VdrInfoAdmin(admin.ModelAdmin):
    pass
    # form = MyUserChangeForm
    # add_form = MyUserCreationForm


@admin.register(Tblsalesorders)
class TblsalesordersAdmin(admin.ModelAdmin):
    list_display = ('id', 'customer', 'term', 'timecreated', 'duedate', 'memo')


# @admin.register(Tblsolines)
# class TblsolinesAdmin(admin.ModelAdmin):
#     list_display = ('id', 'so_id')
