# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models


# class DashboardLogistics(models.Model):
#     team = models.CharField(db_column='Team', max_length=45, blank=True, null=True)  # Field name made lowercase.
#     late = models.IntegerField(db_column='Late', blank=True, null=True)  # Field name made lowercase.
#     date = models.DateTimeField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
#     orders = models.IntegerField(db_column='Orders', blank=True, null=True)  # Field name made lowercase.
#     totalorders = models.IntegerField(db_column='TotalOrders', blank=True, null=True)  # Field name made lowercase.
#
#     class Meta:
#         managed = False
#         db_table = 'dashboard_logistics'
#
#
# class Tblnotes(models.Model):
#     id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
#     po_id = models.IntegerField(db_column='PO_ID', blank=True, null=True)  # Field name made lowercase.
#     description = models.TextField(db_column='Description', blank=True, null=True)  # Field name made lowercase.
#     date = models.DateTimeField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
#
#     class Meta:
#         managed = False
#         db_table = 'tblnotes'
#
#
# class Tblpolines(models.Model):
#     id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
#     po_id = models.IntegerField(db_column='PO_ID', blank=True, null=True)  # Field name made lowercase.
#     itemfullname = models.CharField(db_column='ItemFullName', max_length=50, blank=True, null=True)  # Field name made lowercase.
#     quantity = models.FloatField(db_column='Quantity', blank=True, null=True)  # Field name made lowercase.
#     amount = models.FloatField(db_column='Amount', blank=True, null=True)  # Field name made lowercase.
#     rate = models.FloatField(db_column='Rate', blank=True, null=True)  # Field name made lowercase.
#     description = models.TextField(db_column='Description', blank=True, null=True)  # Field name made lowercase.
#     classname = models.TextField(db_column='ClassName', blank=True, null=True)  # Field name made lowercase.
#     um = models.CharField(db_column='UM', max_length=50, blank=True, null=True)  # Field name made lowercase.
#     customername = models.CharField(db_column='CustomerName', max_length=50, blank=True, null=True)  # Field name made lowercase.
#     manufacturerpartnumber = models.CharField(db_column='ManufacturerPartNumber', max_length=100, blank=True, null=True)  # Field name made lowercase.
#     overrideuomset = models.CharField(db_column='OverrideUOMSet', max_length=100, blank=True, null=True)  # Field name made lowercase.
#     inventorysitelocation = models.CharField(db_column='InventorySiteLocation', max_length=100, blank=True, null=True)  # Field name made lowercase.
#     servicedate = models.DateTimeField(db_column='ServiceDate', blank=True, null=True)  # Field name made lowercase.
#     receivequantity = models.FloatField(db_column='ReceiveQuantity', blank=True, null=True)  # Field name made lowercase.
#     unbilledquantity = models.FloatField(db_column='UnBilledQuantity', blank=True, null=True)  # Field name made lowercase.
#     isbilled = models.IntegerField(db_column='IsBilled', blank=True, null=True)  # Field name made lowercase.
#     ismanuallyclosed = models.IntegerField(db_column='IsManuallyClosed', blank=True, null=True)  # Field name made lowercase.
#     other1 = models.CharField(db_column='Other1', max_length=100, blank=True, null=True)  # Field name made lowercase.
#     other2 = models.CharField(db_column='Other2', max_length=100, blank=True, null=True)  # Field name made lowercase.
#     date = models.DateTimeField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
#     shipdate = models.DateTimeField(db_column='ShipDate', blank=True, null=True)  # Field name made lowercase.
#     received = models.DecimalField(db_column='Received', max_digits=2, decimal_places=0, blank=True, null=True)  # Field name made lowercase.
#     origin = models.CharField(db_column='Origin', max_length=45, blank=True, null=True)  # Field name made lowercase.
#     claim = models.DecimalField(db_column='Claim', max_digits=2, decimal_places=0, blank=True, null=True)  # Field name made lowercase.
#     warehouse = models.DecimalField(db_column='Warehouse', max_digits=2, decimal_places=0, blank=True, null=True)  # Field name made lowercase.
#
#     class Meta:
#         managed = False
#         db_table = 'tblpolines'
#
#
# class Tblpurchaseorders(models.Model):
#     id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
#     txnid = models.CharField(db_column='TxnID', max_length=30, blank=True, null=True)  # Field name made lowercase.
#     timecreated = models.DateTimeField(db_column='TimeCreated', blank=True, null=True)  # Field name made lowercase.
#     timemodified = models.DateTimeField(db_column='TimeModified', blank=True, null=True)  # Field name made lowercase.
#     editsequence = models.CharField(db_column='EditSequence', max_length=50, blank=True, null=True)  # Field name made lowercase.
#     txnnumber = models.CharField(db_column='TxnNumber', max_length=20, blank=True, null=True)  # Field name made lowercase.
#     field_vendor = models.CharField(db_column='_Vendor', max_length=50, blank=True, null=True)  # Field name made lowercase. Field renamed because it started with '_'.
#     field_class = models.CharField(db_column='_Class', max_length=50, blank=True, null=True)  # Field name made lowercase. Field renamed because it started with '_'.
#     inventorysite = models.CharField(db_column='InventorySite', max_length=50, blank=True, null=True)  # Field name made lowercase.
#     shiptoentitysite = models.CharField(db_column='ShipToEntitySite', max_length=50, blank=True, null=True)  # Field name made lowercase.
#     field_template = models.CharField(db_column='_Template', max_length=50, blank=True, null=True)  # Field name made lowercase. Field renamed because it started with '_'.
#     txndate = models.DateTimeField(db_column='TxnDate', blank=True, null=True)  # Field name made lowercase.
#     refnumber = models.CharField(db_column='RefNumber', max_length=50, blank=True, null=True)  # Field name made lowercase.
#     term = models.CharField(db_column='Term', max_length=50, blank=True, null=True)  # Field name made lowercase.
#     duedate = models.DateTimeField(db_column='DueDate', blank=True, null=True)  # Field name made lowercase.
#     expecteddate = models.CharField(db_column='ExpectedDate', max_length=50, blank=True, null=True)  # Field name made lowercase.
#     shipmethod = models.CharField(db_column='ShipMethod', max_length=50, blank=True, null=True)  # Field name made lowercase.
#     currency = models.CharField(db_column='Currency', max_length=20, blank=True, null=True)  # Field name made lowercase.
#     exchangerate = models.FloatField(db_column='ExchangeRate', blank=True, null=True)  # Field name made lowercase.
#     totalamountinhomecurrency = models.FloatField(db_column='TotalAmountInHomeCurrency', blank=True, null=True)  # Field name made lowercase.
#     ismanuallyclosed = models.IntegerField(db_column='IsManuallyClosed', blank=True, null=True)  # Field name made lowercase.
#     isfullyreceived = models.IntegerField(db_column='IsFullyReceived', blank=True, null=True)  # Field name made lowercase.
#     memo = models.TextField(db_column='Memo', blank=True, null=True)  # Field name made lowercase.
#     vendormsg = models.TextField(db_column='VendorMsg', blank=True, null=True)  # Field name made lowercase.
#     istobeprinted = models.IntegerField(db_column='IsToBePrinted', blank=True, null=True)  # Field name made lowercase.
#     istobeemailed = models.IntegerField(db_column='IsToBeEmailed', blank=True, null=True)  # Field name made lowercase.
#     other1 = models.TextField(db_column='Other1', blank=True, null=True)  # Field name made lowercase.
#     other2 = models.TextField(db_column='Other2', blank=True, null=True)  # Field name made lowercase.
#     sotxnid = models.CharField(db_column='SOTxnID', max_length=20, blank=True, null=True)  # Field name made lowercase.
#     sonumber = models.CharField(db_column='SONumber', max_length=20, blank=True, null=True)  # Field name made lowercase.
#     sosalesrep = models.CharField(db_column='SOSalesRep', max_length=20, blank=True, null=True)  # Field name made lowercase.
#     vendoradd1 = models.CharField(db_column='VendorAdd1', max_length=100, blank=True, null=True)  # Field name made lowercase.
#     vendoradd2 = models.CharField(db_column='VendorAdd2', max_length=100, blank=True, null=True)  # Field name made lowercase.
#     vendoradd3 = models.CharField(db_column='VendorAdd3', max_length=100, blank=True, null=True)  # Field name made lowercase.
#     totalamount = models.DecimalField(db_column='TotalAmount', max_digits=15, decimal_places=2, blank=True, null=True)  # Field name made lowercase.
#     fob = models.CharField(db_column='FOB', max_length=20, blank=True, null=True)  # Field name made lowercase.
#     vendorcity = models.CharField(db_column='VendorCity', max_length=50, blank=True, null=True)  # Field name made lowercase.
#     vendorstate = models.CharField(db_column='VendorState', max_length=50, blank=True, null=True)  # Field name made lowercase.
#     vendorzip = models.CharField(db_column='VendorZip', max_length=50, blank=True, null=True)  # Field name made lowercase.
#     vendorcountry = models.CharField(db_column='VendorCountry', max_length=50, blank=True, null=True)  # Field name made lowercase.
#     shipadd1 = models.CharField(db_column='ShipAdd1', max_length=50, blank=True, null=True)  # Field name made lowercase.
#     shipadd2 = models.CharField(db_column='ShipAdd2', max_length=50, blank=True, null=True)  # Field name made lowercase.
#     shipadd3 = models.CharField(db_column='ShipAdd3', max_length=50, blank=True, null=True)  # Field name made lowercase.
#     shipstate = models.CharField(db_column='ShipState', max_length=50, blank=True, null=True)  # Field name made lowercase.
#     shipcity = models.CharField(db_column='ShipCity', max_length=50, blank=True, null=True)  # Field name made lowercase.
#     shipzip = models.CharField(db_column='ShipZip', max_length=50, blank=True, null=True)  # Field name made lowercase.
#     shipcountry = models.CharField(db_column='ShipCountry', max_length=50, blank=True, null=True)  # Field name made lowercase.
#     followup = models.DateTimeField(db_column='Followup', blank=True, null=True)  # Field name made lowercase.
#     status = models.CharField(db_column='Status', max_length=50, blank=True, null=True)  # Field name made lowercase.
#     international = models.CharField(db_column='International', max_length=50, blank=True, null=True)  # Field name made lowercase.
#     urgent = models.CharField(db_column='Urgent', max_length=50, blank=True, null=True)  # Field name made lowercase.
#     shipdate = models.CharField(db_column='Shipdate', max_length=50, blank=True, null=True)  # Field name made lowercase.
#     late = models.CharField(db_column='Late', max_length=45, blank=True, null=True)  # Field name made lowercase.
#
#     class Meta:
#         managed = False
#         db_table = 'tblpurchaseorders'


class Tblsalesorders(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    txnid = models.CharField(db_column='TxnID', max_length=20, blank=True, null=True)  # Field name made lowercase.
    timecreated = models.DateTimeField(db_column='TimeCreated', blank=True, null=True)  # Field name made lowercase.
    timemodified = models.DateTimeField(db_column='TimeModified', blank=True, null=True)  # Field name made lowercase.
    editsequence = models.CharField(db_column='EditSequence', max_length=20, blank=True, null=True)  # Field name made lowercase.
    txnnumber = models.CharField(db_column='TxnNumber', max_length=20, blank=True, null=True)  # Field name made lowercase.
    customer = models.CharField(db_column='Customer', max_length=100, blank=True, null=True)  # Field name made lowercase.
    field_class = models.CharField(db_column='_Class', max_length=100, blank=True, null=True)  # Field name made lowercase. Field renamed because it started with '_'.
    field_template = models.CharField(db_column='_Template', max_length=100, blank=True, null=True)  # Field name made lowercase. Field renamed because it started with '_'.
    txndate = models.DateTimeField(db_column='TxnDate', blank=True, null=True)  # Field name made lowercase.
    refnumber = models.CharField(db_column='RefNumber', max_length=20, blank=True, null=True)  # Field name made lowercase.
    ponumber = models.CharField(db_column='PONumber', max_length=20, blank=True, null=True)  # Field name made lowercase.
    term = models.CharField(db_column='Term', max_length=100, blank=True, null=True)  # Field name made lowercase.
    duedate = models.DateTimeField(db_column='DueDate', blank=True, null=True)  # Field name made lowercase.
    sosalesrep = models.CharField(db_column='SOSalesRep', max_length=100, blank=True, null=True)  # Field name made lowercase.
    shipdate = models.DateTimeField(db_column='ShipDate', blank=True, null=True)  # Field name made lowercase.
    shipmethod = models.CharField(db_column='ShipMethod', max_length=100, blank=True, null=True)  # Field name made lowercase.
    saletaxpercentage = models.FloatField(db_column='SaleTaxPercentage', blank=True, null=True)  # Field name made lowercase.
    saletaxtotal = models.FloatField(db_column='SaleTaxTotal', blank=True, null=True)  # Field name made lowercase.
    totalamount = models.FloatField(db_column='TotalAmount', blank=True, null=True)  # Field name made lowercase.
    billadd1 = models.CharField(db_column='BillAdd1', max_length=100, blank=True, null=True)  # Field name made lowercase.
    billadd2 = models.CharField(db_column='BillAdd2', max_length=100, blank=True, null=True)  # Field name made lowercase.
    billadd3 = models.CharField(db_column='BillAdd3', max_length=100, blank=True, null=True)  # Field name made lowercase.
    billcity = models.CharField(db_column='BillCity', max_length=100, blank=True, null=True)  # Field name made lowercase.
    billstate = models.CharField(db_column='BillState', max_length=100, blank=True, null=True)  # Field name made lowercase.
    billzip = models.CharField(db_column='BillZip', max_length=100, blank=True, null=True)  # Field name made lowercase.
    billcountry = models.CharField(db_column='BillCountry', max_length=50, blank=True, null=True)  # Field name made lowercase.
    shipadd1 = models.CharField(db_column='ShipAdd1', max_length=100, blank=True, null=True)  # Field name made lowercase.
    shipadd2 = models.CharField(db_column='ShipAdd2', max_length=100, blank=True, null=True)  # Field name made lowercase.
    shipadd3 = models.CharField(db_column='ShipAdd3', max_length=100, blank=True, null=True)  # Field name made lowercase.
    shipcity = models.CharField(db_column='ShipCity', max_length=100, blank=True, null=True)  # Field name made lowercase.
    shipstate = models.CharField(db_column='ShipState', max_length=100, blank=True, null=True)  # Field name made lowercase.
    shipzip = models.CharField(db_column='ShipZip', max_length=100, blank=True, null=True)  # Field name made lowercase.
    shipcountry = models.CharField(db_column='ShipCountry', max_length=100, blank=True, null=True)  # Field name made lowercase.
    currency = models.CharField(db_column='Currency', max_length=50, blank=True, null=True)  # Field name made lowercase.
    exchangerate = models.FloatField(db_column='ExchangeRate', blank=True, null=True)  # Field name made lowercase.
    totalamountinhomecurrency = models.FloatField(db_column='TotalAmountInHomeCurrency', blank=True, null=True)  # Field name made lowercase.
    ismanuallyclosed = models.IntegerField(db_column='IsManuallyClosed', blank=True, null=True)  # Field name made lowercase.
    isfullyinvoiced = models.IntegerField(db_column='IsFullyInvoiced', blank=True, null=True)  # Field name made lowercase.
    memo = models.TextField(db_column='Memo', blank=True, null=True)  # Field name made lowercase.
    customermsg = models.TextField(db_column='CustomerMsg', blank=True, null=True)  # Field name made lowercase.
    istobeprinted = models.IntegerField(db_column='IsToBePrinted', blank=True, null=True)  # Field name made lowercase.
    istobeemailed = models.IntegerField(db_column='IsToBeEmailed', blank=True, null=True)  # Field name made lowercase.
    customersaletaxcode = models.CharField(db_column='CustomerSaleTaxCode', max_length=100, blank=True, null=True)  # Field name made lowercase.
    other1 = models.CharField(db_column='Other1', max_length=100, blank=True, null=True)  # Field name made lowercase.
    other2 = models.CharField(db_column='Other2', max_length=100, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'tblsalesorders'


class Tblsalesummaries(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    generateddate = models.DateTimeField(db_column='GeneratedDate', blank=True, null=True)  # Field name made lowercase.
    currentmonth = models.FloatField(db_column='CurrentMonth', blank=True, null=True)  # Field name made lowercase.
    currentyear = models.FloatField(db_column='CurrentYear', blank=True, null=True)  # Field name made lowercase.
    previousyear = models.FloatField(db_column='PreviousYear', blank=True, null=True)  # Field name made lowercase.
    currentmonthlastyear = models.FloatField(db_column='CurrentMonthLastYear', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        # managed = False
        db_table = 'tblsalesummaries'


# class Tblsolines(models.Model):
#     id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
#     so_id = models.ForeignKey(Tblsalesorders, db_column='SO_ID', blank=True, null=True)  # Field name made lowercase.
#     itemfullname = models.CharField(db_column='ItemFullName', max_length=100, blank=True, null=True)  # Field name made lowercase.
#     quantity = models.FloatField(db_column='Quantity', blank=True, null=True)  # Field name made lowercase.
#     amount = models.FloatField(db_column='Amount', blank=True, null=True)  # Field name made lowercase.
#     rate = models.FloatField(db_column='Rate', blank=True, null=True)  # Field name made lowercase.
#     ratepercent = models.FloatField(db_column='RatePercent', blank=True, null=True)  # Field name made lowercase.
#     description = models.TextField(db_column='Description', blank=True, null=True)  # Field name made lowercase.
#     classname = models.CharField(db_column='ClassName', max_length=100, blank=True, null=True)  # Field name made lowercase.
#     um = models.CharField(db_column='UM', max_length=100, blank=True, null=True)  # Field name made lowercase.
#     inventorysite = models.CharField(db_column='InventorySite', max_length=100, blank=True, null=True)  # Field name made lowercase.
#     inventorysitelocation = models.CharField(db_column='InventorySiteLocation', max_length=100, blank=True, null=True)  # Field name made lowercase.
#     ismanuallyclosed = models.IntegerField(db_column='IsManuallyClosed', blank=True, null=True)  # Field name made lowercase.
#     other1 = models.CharField(db_column='Other1', max_length=100, blank=True, null=True)  # Field name made lowercase.
#     other2 = models.CharField(db_column='Other2', max_length=100, blank=True, null=True)  # Field name made lowercase.
#     invoiced = models.FloatField(db_column='Invoiced', blank=True, null=True)  # Field name made lowercase.
#
#     class Meta:
#         managed = False
#         db_table = 'tblsolines'


class VdrInfo(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    vdr_id = models.CharField(db_column='VDR_ID', max_length=45, blank=True, null=True)  # Field name made lowercase.
    c1 = models.CharField(db_column='C1', max_length=45, blank=True, null=True)  # Field name made lowercase.
    p1 = models.CharField(db_column='P1', max_length=45, blank=True, null=True)  # Field name made lowercase.
    e1 = models.CharField(db_column='E1', max_length=45, blank=True, null=True)  # Field name made lowercase.
    p2 = models.CharField(db_column='P2', max_length=45, blank=True, null=True)  # Field name made lowercase.
    e2 = models.CharField(db_column='E2', max_length=45, blank=True, null=True)  # Field name made lowercase.
    c3 = models.CharField(db_column='C3', max_length=45, blank=True, null=True)  # Field name made lowercase.
    p3 = models.CharField(db_column='P3', max_length=45, blank=True, null=True)  # Field name made lowercase.
    e3 = models.CharField(db_column='E3', max_length=45, blank=True, null=True)  # Field name made lowercase.
    c4 = models.CharField(db_column='C4', max_length=45, blank=True, null=True)  # Field name made lowercase.
    p4 = models.CharField(db_column='P4', max_length=45, blank=True, null=True)  # Field name made lowercase.
    e4 = models.CharField(db_column='E4', max_length=45, blank=True, null=True)  # Field name made lowercase.
    c5 = models.CharField(db_column='C5', max_length=45, blank=True, null=True)  # Field name made lowercase.
    p5 = models.CharField(db_column='P5', max_length=45, blank=True, null=True)  # Field name made lowercase.
    e5 = models.CharField(db_column='E5', max_length=45, blank=True, null=True)  # Field name made lowercase.
    c2 = models.CharField(db_column='C2', max_length=45, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'vdr_info'
